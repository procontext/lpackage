<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 19.10.18
 * Time: 11:26
 */

return [
    'mailer' => [
        'driver'    => 'mysql',
        'host'      => env('DB_HOST'),
        'port'      => env('DB_PORT'),
        'database'  => env('MAILER_DB'),
        'username'  => env('MAILER_USERNAME'),
        'password'  => env('MAILER_PASSWORD'),
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
        'strict'    => false
    ],
];
