<?php

return [

    'config' => 1,

    'instant' => [
        'host' => env('MAIL_HOST'),
        'port' => env('MAIL_PORT'),
        'encryption' => env('MAIL_ENCRYPTION'),
        'email' => env('MAIL_EMAIL'),
        'password' => env('MAIL_PASSWORD'),
    ],

    'title' => 'Example Title',

    'agency_recipients' => [
        'example@examle.ru' => 'Example Name'
    ],

    'client_recipients' => [
        'example@examle.ru' => 'Example Name'
    ],
];