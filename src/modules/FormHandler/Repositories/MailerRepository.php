<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.11.18
 * Time: 11:43
 */

namespace Procontext\LPackage\Modules\FormHandler\Repositories;

use Illuminate\Database\Eloquent\Model;
use Procontext\LPackage\Modules\FormHandler\Entities\Mail\Mail;

/**
 * Class UserRepository
 * @package App\Models\User
 * @property $id
 * @property $config
 * @property $title
 * @property $subject
 * @property $recipients
 * @property $body
 * @property $date
 * @property $status
 * @property $attempt_cnt
 * @property $error
 */
class MailerRepository extends Model {

    public $table = 'mails';

    public $incrementing = true;
    public $timestamps = false;

    protected $connection = 'mailer';

    protected $fillable = [
        'config', 'title', 'subject', 'recipients', 'body'
    ];

    protected $casts = [
        'recipients' => 'array'
    ];

    /**
     * @param Mail $mail
     * @return mixed
     */
    public function saveEntity(Mail $mail) {
        if(!$mail->getRecipients()) return true;

        return self::query()->insert([
            'config' => $mail->getConfig(),
            'title' => $mail->getTitle(),
            'subject' => $mail->getSubject(),
            'recipients' => json_encode($mail->getRecipients(), JSON_UNESCAPED_UNICODE),
            'body' => $mail->getBody()
        ]);

    }

}