<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 20.11.18
 * Time: 12:47
 */

namespace Procontext\LPackage\Modules\FormHandler\Entities\Mail;

use Procontext\LPackage\Modules\FormHandler\Entities\Form\SimpleAutoForm;

/**
 * Class SimpleAutoMail
 * @package Procontext\LPackage\Modules\FormHandler\Mail
 */
class SimpleAutoMail extends Mail {

    /**
     * @param SimpleAutoForm $form
     * @param bool $extra
     * @param mixed ...$options
     */
    public function setBody($form, $extra = false, ...$options)
    {
        $br = "<br>";
        $urlLink = "<a href='" . $form->getUrl() . "'>" . $form->getUrl() . "</a>";
        $body = $urlLink . $br . $br .
            'Заполнена форма ' . $this->bold($this->getTypeName($form->getType())) . $br .
            '=============================' . $br .
            'Номер клиента  - ' . $this->bold($form->getPhone()) . $br .
            '=============================' . $br .
            'Адрес страницы - ' . $form->getUrlWithQueryString() . $br .
            '=============================' . $br .
            'Placement  - ' .$this->bold($form->getPath()) . $br;

        if($extra) {
            $ip = $options[0];

            $body .=
            '=============================' . $br .
            'Ip:' . $br . $ip . $br .
            '=============================' . $br .
            'CallKeeper Log :' . $br .  str_replace("\n", $br ,$form->getCkMessage()) . $br;
        }

        $this->body = $body;
    }


    public function setSubjectByType($type, $status = true)
    {
        $prefix = $status ? '' : 'Ошибка!';

        switch($type)
        {
            case 1: $this->subject = $prefix . 'Заявка в отдел продаж';break;
            case 2: $this->subject = $prefix . 'Заявка в отдел кредитования';break;
            case 3: $this->subject = $prefix . 'Заявка в отдел сервиса';break;
            case 4: $this->subject = $prefix . 'Заявка на тест-драйв';break;
        }
    }

    /**
     * @param int $type
     * @return string
     */
    protected function getTypeName(int $type) {
        switch ($type) {
            case 1:
                return 'Продажа';
            case 2:
                return 'Кредит';
            case 3:
                return 'Сервис';
            case 4:
                return 'Тест-драйв';
            default:
                return 'Продажа';
        }
    }

}