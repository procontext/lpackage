<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 20.11.18
 * Time: 13:25
 */

namespace Procontext\LPackage\Modules\FormHandler\Entities\Mail;


/**
 * Class Mail
 * @package Procontext\LPackage\Modules\FormHandler\Entities\Mail
 */
abstract class Mail {

    protected $config;
    protected $title;
    protected $subject = '';
    protected $recipients = [];
    protected $body = '';


    /**
     * Mail constructor.
     */
    public function __construct() {
        $this->config = config('mailer.config');
        $this->title = config('mailer.title');
    }

    /**
     * @return int
     */
    public function getConfig(): int {
        return $this->config;
    }


    /**
     * @param int $config
     */
    public function setConfig(int $config) {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }


    /**
     * @param $form
     * @param bool $extra
     * @param mixed ...$options
     */
    abstract public function setBody($form, $extra = false, ...$options);


    /**
     * @return array
     */
    public function getRecipients(): array {
        return $this->recipients;
    }


    /**
     * @param array $recipients
     */
    public function setRecipients(array $recipients) {
        $this->recipients = $recipients;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
    }

    /**
     * @param string $string
     * @return string
     */
    protected function bold(string $string)
    {
        return "<b>" . $string  . "</b>";
    }

}