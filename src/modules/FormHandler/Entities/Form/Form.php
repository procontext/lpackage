<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 20.11.18
 * Time: 14:36
 */

namespace Procontext\LPackage\Modules\FormHandler\Entities\Form;


/**
 * Class Form
 * @package Procontext\LPackage\Modules\FormHandler\Entities\Form
 */
abstract class Form {

    protected $url;
    protected $path;
    protected $debug;

    /**
     * @param array $data
     */
    public function setData(array $data) {
        $this->url = array_get($data, 'url', '');
        $this->path = array_get($data, 'path', '');
        $this->debug = array_get($data, 'debug', 0);
    }


    public function toArray() {
        return [
            'url' => $this->url,
            'path' => $this->path,
            'debug' => $this->debug
        ];
    }

    /**
     * @return string
     */
    public function getUrl(): string {
        return strtok($this->url,'?');
    }

    /**
     * @return string
     */
    public function getUrlWithQueryString(): string {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getPath(): string {
        return $this->path;
    }

    /**
     * @return bool
     */
    public function isDebug(): bool {
        return (bool) $this->debug;
    }
}