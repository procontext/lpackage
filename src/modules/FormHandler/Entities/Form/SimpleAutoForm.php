<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 19.11.18
 * Time: 19:16
 */

namespace Procontext\LPackage\Modules\FormHandler\Entities\Form;

/**
 * Class SimpleAutoForm
 * @package Procontext\LPackage\Modules\FormHandler\Entities\Form
 */
class SimpleAutoForm extends SimpleForm {

    protected $type;

    /**
     * @param array $data
     */
    public function setData(array $data) {
        parent::setData($data);
        $this->type = $data['type'];
    }

    public function toArray() {
        return array_merge(parent::toArray(),[
            'type' => $this->type
        ]);
    }

    /**
     * @return int
     */
    public function getType(): int {
        return $this->type;
    }

}


