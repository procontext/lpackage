<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 19.11.18
 * Time: 19:16
 */

namespace Procontext\LPackage\Modules\FormHandler\Entities\Form;


/**
 * Class SimpleForm
 * @package Procontext\LPackage\Modules\FormHandler\Entities\Form
 */
class SimpleForm extends Form {

    const TYPE = 0;

    protected $phone;
    protected $ckStatus;
    protected $ckMessage;

    /**
     * @param array $data
     */
    public function setData(array $data) {
        parent::setData($data);
        $this->phone = array_get($data, 'phone', 1);
        $this->ckStatus = (bool) array_get($data, 'ck_log.status', true);
        $this->ckMessage = array_get($data, 'ck_log.message', '');
    }


    public function toArray() {
        return array_merge(parent::toArray(),[
            'phone' => $this->phone,
        ]);
    }

    /**
     * @return string
     */
    public function getPhone(): string {
        return $this->phone;
    }


    /**
     * @return bool
     */
    public function getCkStatus(): bool {
        return (bool) $this->ckStatus;
    }

    /**
     * @return string
     */
    public function getCkMessage(): string {
        return $this->ckMessage;
    }
}

