<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.11.18
 * Time: 11:57
 */

namespace Procontext\LPackage\Modules\FormHandler\Requests;

use Urameshibr\Requests\FormRequest as BaseFormRequest;

abstract class FormRequest extends BaseFormRequest {

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'url' => 'url|nullable',
            'path' => 'string|nullable',
            'debug' => 'int|between:0,1|nullable'
        ];
    }
}

