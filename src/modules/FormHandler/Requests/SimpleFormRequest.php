<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.11.18
 * Time: 12:16
 */

namespace Procontext\LPackage\Modules\FormHandler\Requests;

class SimpleFormRequest extends FormRequest {

    public function rules()
    {
        return array_merge(parent::rules(),[
            'phone' => 'required|string|regex:/[0-9]{11}/',
            'ck_log' => 'array|nullable',
            'ck_log.status' => 'int|nullable',
            'ck_log.message' => 'string|nullable'
        ]);
    }

}