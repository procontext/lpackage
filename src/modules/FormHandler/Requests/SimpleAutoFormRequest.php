<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.11.18
 * Time: 12:16
 */

namespace Procontext\LPackage\Modules\FormHandler\Requests;

class SimpleAutoFormRequest extends SimpleFormRequest {

    public function rules()
    {
        return array_merge(parent::rules(),[
            'type' => 'required|int|between:0,4'
        ]);
    }

}