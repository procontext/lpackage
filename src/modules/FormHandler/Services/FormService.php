<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.12.18
 * Time: 10:51
 */

namespace Procontext\LPackage\Modules\FormHandler\Services;

use Procontext\LPackage\Modules\FormHandler\Entities\Form\Form;
use Procontext\LPackage\Modules\FormHandler\Entities\Mail\Mail;
use Procontext\LPackage\Modules\FormHandler\Requests\FormRequest;

/**
 * Class FormService
 * @package Procontext\LPackage\Modules\FormHandler\Services
 */
abstract class FormService {


    /**
     * @param FormRequest $request
     * @return array
     */
    public function handle(FormRequest $request): array {
        $formType = $request->input('form', 0);

        $form = $this->form($formType);
        $form->setData($request->all());

        if($form->isDebug()) return ['debug' => true];

        $mail = $this->mail($formType);

        return $this->send($mail, $form);
    }


    public function getIp() {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }

    /**
     * @param int $formType
     * @return Form
     */
    abstract protected function form(int $formType = 0): Form;


    /**
     * @param int $formType
     * @return Mail
     */
    abstract protected function mail(int $formType = 0): Mail;

    /**
     * @param $mail
     * @param $form
     * @return array
     */
    abstract protected function send($mail, $form): array;

}