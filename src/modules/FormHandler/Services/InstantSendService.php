<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 01.02.19
 * Time: 11:20
 */

namespace Procontext\LPackage\Modules\FormHandler\Services;

use Procontext\LPackage\Modules\FormHandler\Entities\Form\Form;
use Procontext\LPackage\Modules\FormHandler\Entities\Form\SimpleAutoForm;
use Procontext\LPackage\Modules\FormHandler\Entities\Mail\Mail;
use Procontext\LPackage\Modules\FormHandler\Entities\Mail\SimpleAutoMail;
use Procontext\LPackage\Modules\FormHandler\Requests\FormRequest;

class InstantSendService extends FormService {

    /** @var \Swift_Mailer $mailer*/
    protected $mailer;

    public function __construct() {
        $transport = new \Swift_SmtpTransport();

        $transport->setUsername(config('mailer.instant.email'))
            ->setPassword(config('mailer.instant.password'))
            ->setPort(config('mailer.instant.port'))
            ->setHost(config('mailer.instant.host'))
            ->setEncryption(config('mailer.instant.encryption'));

        $this->mailer = new \Swift_Mailer($transport);
    }


    /**
     * @param FormRequest $request
     * @return array
     */
    public function handle(FormRequest $request): array {
        $formType = $request->input('form', 0);

        $form = $this->form($formType);
        $form->setData($request->all());

        if($form->isDebug()) return ['debug' => true];

        $mail = $this->mail($formType);

        return $this->send($mail, $form);
    }

    /**
     * @param int $formType
     * @return Form
     */
    protected function form(int $formType = 0): Form {
        return new SimpleAutoForm();
    }

    /**
     * @param int $formType
     * @return Mail
     */
    protected function mail(int $formType = 0): Mail {
        return new SimpleAutoMail();
    }



    /**
     * @param SimpleAutoMail $mail
     * @param SimpleAutoForm $form
     * @return array
     */
    protected function send($mail, $form): array {
        return [
            'mail' => $this->sendAgency($mail, $form) && $this->sendClient($mail, $form)
        ];
    }

    /**
     * @param SimpleAutoMail $mail
     * @param SimpleAutoForm $form
     * @return bool
     */
    protected function sendAgency($mail, $form): bool {
        $mail->setSubjectByType($form->getType(), $form->getCkStatus());
        $mail->setBody($form, true);
        $mail->setRecipients(config('mailer.agency_recipients'));
        return $this->sendMessage($mail);
    }

    /**
     * @param SimpleAutoMail $mail
     * @param SimpleAutoForm $form
     * @return bool
     */
    protected function sendClient($mail, $form): bool {
        $mail->setSubjectByType($form->getType());
        $mail->setBody($form, false);
        $mail->setRecipients(config('mailer.client_recipients'));
        return $this->sendMessage($mail);
    }


    /**
     * @param SimpleAutoMail $mail
     * @return bool
     */
    protected function sendMessage($mail): bool {
        $message = new \Swift_Message();
        $message->setFrom(config('mailer.instant.email'), $mail->getTitle());
        $message->setTo($mail->getRecipients());
        $message->setSubject($mail->getSubject());
        $message->setBody($mail->getBody(), 'text/html');

        return !empty($mail->getRecipients()) ? $this->mailer->send($message) : true;
    }
}
