<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 23.11.18
 * Time: 11:35
 */

namespace Procontext\LPackage\Modules\FormHandler\Services;

use Procontext\LPackage\Modules\FormHandler\Entities\Form\Form;
use Procontext\LPackage\Modules\FormHandler\Entities\Form\SimpleAutoForm;
use Procontext\LPackage\Modules\FormHandler\Entities\Mail\Mail;
use Procontext\LPackage\Modules\FormHandler\Entities\Mail\SimpleAutoMail;
use Procontext\LPackage\Modules\FormHandler\Repositories\MailerRepository;

/**
 * Class FormService
 * @package App\Services
 */
class FormAutoService extends FormService {

    /**
     * @var MailerRepository
     */
    protected $repository;

    /**
     * FormService constructor.
     * @param MailerRepository $repository
     */
    public function __construct(MailerRepository $repository) {
        $this->repository = $repository;
    }


    /**
     * @param int $formType
     * @return Form
     */
    protected function form(int $formType = 0): Form {
        return new SimpleAutoForm();
    }

    /**
     * @param int $formType
     * @return Mail
     */
    protected function mail(int $formType = 0): Mail {
        return new SimpleAutoMail();
    }

    /**
     * @param SimpleAutoMail $mail
     * @param SimpleAutoForm $form
     * @return array
     */
    protected function send($mail, $form): array {
        return [
            'mail' => $this->sendAgency($mail, $form) && $this->sendClient($mail, $form)
        ];
    }

    /**
     * @param SimpleAutoMail $mail
     * @param SimpleAutoForm $form
     * @return bool
     */
    protected function sendAgency($mail, $form): bool {
        $mail->setSubjectByType($form->getType(), $form->getCkStatus());
        $mail->setBody($form, true, $this->getIp());
        $mail->setRecipients(config('mailer.agency_recipients'));
        return $this->repository->saveEntity($mail);
    }

    /**
     * @param SimpleAutoMail $mail
     * @param SimpleAutoForm $form
     * @return bool
     */
    protected function sendClient($mail, $form): bool {
        $mail->setSubjectByType($form->getType());
        $mail->setBody($form, false);
        $mail->setRecipients(config('mailer.client_recipients'));
        return $this->repository->saveEntity($mail);
    }
}