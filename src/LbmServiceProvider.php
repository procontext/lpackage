<?php

namespace Procontext\LPackage;

use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laravelista\LumenVendorPublish\VendorPublishCommand;
use Urameshibr\Providers\FormRequestServiceProvider;

class LbmServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/mailer.php', 'mailer');
        $this->mergeConfigFrom(__DIR__ . '/config/database.php', 'database.connections');

        $this->publishes([
            __DIR__ . '/config' => base_path('config'),
        ]);

        $dir = __DIR__ . '/modules';
        $modules = $this->getModules($dir);

        foreach ($modules as $module) {

            //Подключаем роуты для модуля
            if(file_exists($dir.'/'.$module.'/Routes/routes.php')) {
                $this->loadRoutesFrom($dir.'/'.$module.'/Routes/routes.php');
            }
            //Подгружаем миграции
            if(is_dir($dir.'/'.$module.'/Migration')) {
                $this->loadMigrationsFrom($dir.'/'.$module.'/Migration');
            }
            //Подгружаем переводы
            if(is_dir($dir.'/'.$module.'/Lang')) {
                $this->loadTranslationsFrom($dir.'/'.$module.'/Lang', $module);
            }
        }

    }

    public function register()
    {
        $this->app->register(FormRequestServiceProvider::class);
        $this->app->register(MailServiceProvider::class);
        $this->commands([
            VendorPublishCommand::class
        ]);
    }

    protected function getModules($dir): array {
        $modules = [];

        $files = scandir($dir);
        foreach ($files as $fileName) {
            $file = $dir . '/'. $fileName;
            if(in_array($fileName, ['.', '..']) || !is_dir( $file)) continue;
            $modules[] = $fileName;
        }
        return $modules;
    }
}
